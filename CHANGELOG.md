# Changelog

All notable changes to this project will be documented in this file.

- Update icons from upstream (!4)
- Update icons from upstream (!3)
- 2023-06-18
    - Include styling directly in `meta.icon` strings (!1)
- 2023-06-17
    - Initial release

